class AddRefugeeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :refugee, :boolean, :default => false
  end
end
