class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.text :description
      t.bigint :fund
      t.bigint :funded
      t.date :deadline

      t.timestamps
    end
  end
end
