class CreateRefugees < ActiveRecord::Migration[5.0]
  def change
    create_table :refugees do |t|
      t.string :name
      t.text :story
      t.integer :refugeeID

      t.timestamps
    end
  end
end
