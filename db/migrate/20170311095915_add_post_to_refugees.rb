class AddPostToRefugees < ActiveRecord::Migration[5.0]
  def change
    add_column :refugees, :post, :text
  end
end
