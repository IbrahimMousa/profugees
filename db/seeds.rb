# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create!(name:  "Ibrahim Mousa",
             email: "ibrahimmousa1996@hotmail.com",
             password:              "password",
             password_confirmation: "password",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name: "Hassan Alnouri",
             email: "pvmsano@gmail.com",
             password: "harambe4ever",
             password_confirmation: "harambe4ever",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  "Aya Alnouri",
             email: "ayaalnouri98@yahoo.com",
             password:              "Admin382!",
             password_confirmation: "Admin382!",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

# User.create!(name:  "Nada Hamad",
#              email: "nadahamad988@hotmail.com",
#              password:              "Admin382!",
#              password_confirmation: "Admin382!",
#              admin: true,
#              activated: true,
#              activated_at: Time.zone.now)

User.create!(name:  "Zena Ayman",
             email: "zenaayman1998@gmail.com",
             password:              "Admin382!",
             password_confirmation: "Admin382!",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end