class ProjectsController < ApplicationController
	before_action :logged_in_user, only: [:edit, :update, :destroy]
	#Remember to Edit this!!

  def new
  	@project = Project.new
  end

  def index
  	@projects = Project.paginate(page: params[:page])
  end

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end


end
