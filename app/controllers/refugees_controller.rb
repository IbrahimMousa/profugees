class RefugeesController < ApplicationController
  before_action :admin_user, only: [:destroy, :new, :create, :edit, :update]

  def index
    @refugees = Refugee.paginate(page: params[:page])
  end

  def show
    @refugee = Refugee.find(params[:id])
  end

  def new
    @refugee = Refugee.new
  end

  def create
    @refugee = Refugee.new(refugee_params)
    if @refugee.save
      flash[:success] = "Refugee added successfully!"
      redirect_to @refugee
    else
      render 'new'
    end
  end

  def edit
    @refugee = Refugee.find(params[:id])
  end

  def update
    @refugee = Refugee.find(params[:id])
    if @refugee.update_attributes(refugee_params)
      flash[:success] = "Info updated"
      redirect_to @refugee
    else
      render 'edit'
    end
  end

  def destroy
    Refugee.find(params[:id]).destroy
    flash[:success] = "Refugee deleted"
    redirect_to refugees_url
  end

  def simpleform
    
  end

private

  def refugee_params
    params.require(:refugee).permit(:name, :story, :refugeeID, :post)
  end


  # Before filters

  # Confirms a logged-in user.
  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

  # Confirms the correct user.
  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user.admin?
  end

  # Confirms an admin user
  def admin_user
      redirect_to(root_url) unless current_user.admin?
  end

end