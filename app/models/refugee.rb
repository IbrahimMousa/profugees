class Refugee < ApplicationRecord
  validates :name,      presence: true, length: { maximum: 50 }
  validates :refugeeID, presence: true, uniqueness: true
  validates :story,     presence: true
end
