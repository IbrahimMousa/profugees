class Project < ApplicationRecord
	validates :name, presence: true, length: { maximum: 30}
	validates :description, presence: true
	validates :fund, presence: true
	validates :deadline, presence: true

end
