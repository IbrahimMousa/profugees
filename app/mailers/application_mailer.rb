class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@profugees.com'
  layout 'mailer'
end
