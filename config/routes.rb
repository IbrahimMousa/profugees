  Rails.application.routes.draw do


  get 'projects/new'

  get 'password_resets/new'

  get 'password_resets/edit'

  get 'refugees/new'

  root 'static_pages#home'

  get '/help',         to: 'static_pages#help'
  get  '/about',       to: 'static_pages#about'
  get '/contact',      to: 'static_pages#contact'
  get '/signup',       to: 'users#new'
  post '/signup',      to: 'users#create'
  get    '/login',     to: 'sessions#new'
  post   '/login',     to: 'sessions#create'
  delete '/logout',    to: 'sessions#destroy'
  get  '/addRefugee',  to: 'refugees#new'
  post '/addRefugee',  to: 'refugees#create'
  get '/form',         to: 'static_pages#form'
  get '/newproject',   to: 'projects#new'

  resources :users
  resources :refugees
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :projects,            only: [:create, :destroy]
  resources :microposts,          only: [:create, :destroy]


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
