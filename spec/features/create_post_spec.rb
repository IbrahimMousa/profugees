require 'rails helper'

feature "user creates blog post" do

	scenario "user creates a post" do
		visit new_post_path
		fill_in "post[title]", with: Faker::Book.title
		fill_in "post[author]", with: Faker::Book.author
		fill_in "post[body]", with: "This is a great post."
		click_button "Create Post"

		expect(Post.count).to eq 1
		post = post.last
		expect(page).to have_text(post.title)
	end
	
end